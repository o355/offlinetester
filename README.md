# Offline Tester
A quick script to turn on/off my wifi interface on my laptop. Used for debugging error catching when offline.

This script is adapted to work on Ubuntu 16.04 LTS (KDE Neon). 

# Setup
Download the script, make sure you have appJar installed. Edit the script to change the interface name to your Wi-Fi/Ethernet interface. Launch the script with admin priviledges.
